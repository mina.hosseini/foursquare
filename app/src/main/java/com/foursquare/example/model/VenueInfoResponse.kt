package com.foursquare.example.model

import com.foursquare.example.app.Constants

class VenueInfoResponse(
    val response: InfoResponse
)

data class InfoResponse(
    val venue: InfoVenue
) {
    var photoURLs: ArrayList<String> = arrayListOf()
        get() = photoURLs()
        set(value) {
            field = value
        }

    fun photoURLs(): ArrayList<String> {
        val photoURLs = ArrayList<String>()

        photoURLs.add(venue.bestPhoto.imageURL)
        if (venue.listed.groups.isNotEmpty())
            if (venue.listed.groups[0].items.isNotEmpty())
                venue.listed.groups[0].items.forEach {
                    it.photo?.imageURL?.let { it1 -> photoURLs.add(it1) }
                }
        if (photoURLs.isEmpty())
            photoURLs.add(Constants.DEFAULT_IMAGE_URL)

        return photoURLs
    }
}

data class InfoVenue(
    val id: String,
    val name: String,
    val contact: Contact,
    val location: InfoLocation,
    val categories: List<InfoCategory>,
    val stats: InfoStats,
    val likes: InfoLikes,
    val hereNow: InfoHereNow,
    val listed: Listed,
    val bestPhoto: Photo
)

data class Photo(
    val prefix: String,
    val suffix: String
) {
    val imageURL get() = prefix + "500x300" + suffix
}

data class InfoCategory(
    val name: String
)

data class Contact(
    val phone: String,
    val instagram: String
) {
    val formattedPhone get() = "Phone: $phone"
    val formattedInstagram get() = "Instagram: $instagram"
}

data class InfoHereNow(
    val count: Long
) {
    val numberOfHereNow get() = "Here now: $count"
}

data class InfoLikes(
    val count: Long
) {
    val numberOfLikes get() = "Number of likes: $count"
}

data class HereNowGroup(
    val items: List<FluffyItem>
)

data class FluffyItem(
    val photo: Photo? = null
)

data class Listed(
    val groups: List<HereNowGroup>
)

data class InfoLocation(
    val address: String
)

data class InfoStats(
    val visitsCount: Long
) {
    val numberOfVisits get() = "Number of visits: $visitsCount"
}


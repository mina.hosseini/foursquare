package com.foursquare.example.model

import java.io.Serializable

class VenuesExploreRes(
    val response: Response
): Serializable

data class Response(
    val groups: List<Group>
): Serializable

data class Group(
    val items: List<GroupItem>
): Serializable

data class GroupItem(
    val venue: Venue
): Serializable

data class Venue(
    val id: String,
    val name: String,
    val location: Location,
    val categories: List<Category>
) : Serializable {
    val iconURL get() = categories.get(0).icon.prefix + "88" + categories.get(0).icon.suffix
}

data class Category(
    val icon: Icon
) : Serializable

data class Icon(
    val prefix: String,
    val suffix: String
):  Serializable

data class Location(
    val address: String,
    val distance: Long
) :  Serializable{
    val distanceStr get() = distance.toString() + "m"
}


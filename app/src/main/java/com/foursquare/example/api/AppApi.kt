package com.foursquare.example.api

import com.foursquare.example.model.*
import retrofit2.http.*

interface AppApi {

    @GET("venues/explore")
    suspend  fun venuesExplore(
    @Query("client_id") clientId: String?,
    @Query("client_secret") clientSecret: String?,
    @Query("v") v: String?,
    @Query("ll") ll: String?,
    @Query("radius") radius: String?,
    @Query("limit") limit: String?,
    @Query("offset") offset: Int?
    ): VenuesExploreRes

    @GET("venues/{id}")
    suspend fun getVenueInfo(
        @Path("id" , encoded = true) term: String,
        @Query("client_id") clientId: String?,
        @Query("client_secret") clientSecret: String?,
        @Query("v") v: String?
    ) : VenueInfoResponse

}
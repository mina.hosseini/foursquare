package com.foursquare.example.api

import android.util.Log
import com.foursquare.example.di.inject
import retrofit2.HttpException
import java.net.SocketTimeoutException

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1)
}

object ResponseHandler {
    fun <T : Any> handleSuccess(data: T): Resource<T> {

        return Resource.success(data)
    }

    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {
            is HttpException -> Resource.error(getErrorMessage(e.code()),e)
            is SocketTimeoutException -> Resource.error(getErrorMessage(ErrorCodes.SocketTimeOut.code), null)
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            ErrorCodes.SocketTimeOut.code -> "No responce found(408)"
            401 -> "Unauthorised"
            404 -> "Nothing found"
            429 -> "Too Many Requests"
            else -> "Connection error"
        }
    }
}

suspend fun <T : Any> apiCall(apiCal: suspend (AppApi.() -> T)): Resource<T> {
    return try {
        val api by inject<AppApi>()
        val response = api.apiCal()
        ResponseHandler.handleSuccess(response)
    } catch (e: Exception) {
        Log.d("api", "failed: $e" )
        e.printStackTrace()
        ResponseHandler.handleException(e)
    }
}

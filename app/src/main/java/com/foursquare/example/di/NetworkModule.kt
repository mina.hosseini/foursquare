package com.foursquare.example.di

import android.content.Context
import com.google.gson.GsonBuilder
import com.foursquare.example.BuildConfig
import com.foursquare.example.api.AppApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.Proxy
import java.util.concurrent.TimeUnit

val networkModule = module {

    factory { GsonBuilder().create() }
    factory { provideAppApi(get()) }

}

fun provideAppApi(context: Context): AppApi {
    val baseUrl = "https://api.foursquare.com/v2/"
    val okHttpClientBuilder = OkHttpClient()
            .newBuilder()
            .proxy(Proxy.NO_PROXY)

    okHttpClientBuilder.callTimeout(30, TimeUnit.SECONDS)
    okHttpClientBuilder.readTimeout( 30, TimeUnit.SECONDS)
    okHttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS)

    if (BuildConfig.DEBUG) {
        okHttpClientBuilder.addInterceptor(
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        )
    }

    val okHttpClient = okHttpClientBuilder.build()

    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(AppApi::class.java)
}

inline val api : AppApi get(){
    val ans : AppApi by inject()
    return ans
}

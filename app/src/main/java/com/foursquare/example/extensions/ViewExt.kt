package com.foursquare.example.extensions

import android.app.Activity
import android.graphics.*
import android.os.Build
import android.view.*

fun View.observeGlobalLayoutOnce(observer: ()->Unit) =
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        override fun onGlobalLayout() {
            try {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                observer()
            } catch (ignored: Exception) {
            }
        }
    })

fun CharSequence.getTimeNeededToRead(): Int {
    // Readable words per minute
    val wordPerMinutes = 180
    val minuteInMillis = 60 * 1000

    // Standardized number of chars in calculable word
    val wordLength = 5
    val wordCount = this.length / wordLength
    val readTime = (wordCount / wordPerMinutes) * minuteInMillis

    // Milliseconds before user starts reading the notification
    val startDelay = 1500

    // Extra time
    val endDelay = 1300

    return startDelay + readTime + endDelay
}

fun Activity.makeStatusBarTransparent() {
    window.apply {
        clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
        statusBarColor = Color.TRANSPARENT
    }
}


package com.foursquare.example.extensions

import com.google.gson.Gson
import com.foursquare.example.di.inject
import org.json.JSONObject
import java.lang.reflect.Type

val gson : Gson by inject()
val Any.json: String get() = gson.toJson(this)

fun <T> String.fromJson(cls: Type): T? {
    try {
        return  gson.fromJson<T>(this, cls)
    }catch (e:Exception){}
    return null
}

@Suppress("unused")
inline fun <reified T> String.fromJson(): T? {
    return fromJson(T::class.java)
}

fun String.jsonChild(child: String) = JSONObject(this).getJSONObject(child).toString()

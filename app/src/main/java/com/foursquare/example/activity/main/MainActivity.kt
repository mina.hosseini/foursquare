package com.foursquare.example.activity.main

import androidx.activity.viewModels
import com.foursquare.example.R
import com.foursquare.example.activity.ArchBaseActivity
import com.foursquare.example.databinding.ActivityMainBinding


class MainActivity :  ArchBaseActivity<ActivityMainBinding, MainActivityVM>() {

    override fun layout() = R.layout.activity_main
    override val viewModel: MainActivityVM by viewModels()
    override fun getNavHostFragmentId() = R.id.main_nav_host

}
package com.foursquare.example.activity

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.databinding.*
import androidx.lifecycle.ViewModel
import com.foursquare.example.extensions.*
import com.phelat.navigationresult.FragmentResultActivity
import kotlinx.coroutines.*


abstract class ArchBaseActivity<B : ViewDataBinding, VM : ViewModel> : FragmentResultActivity(), CoroutineScope by CoroutineScope(Dispatchers.Main) {

    @LayoutRes abstract fun layout(): Int
    protected abstract val viewModel: VM

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layout())

        binding.lifecycleOwner = this

        binding.setView(this)
        binding.setVm(viewModel)

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

}
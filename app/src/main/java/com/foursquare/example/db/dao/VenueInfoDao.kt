package com.foursquare.example.db.dao

import androidx.room.*
import com.foursquare.example.db.table.DBVenueInfo

@Dao
interface VenueInfoDao {

    @Query("SELECT * from info WHERE id = :id")
    suspend fun getVenueInfo(id: String): DBVenueInfo

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(venue: DBVenueInfo)

}

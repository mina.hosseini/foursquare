package com.foursquare.example.db.table

import android.os.Parcelable
import androidx.room.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.parcelize.Parcelize

@Entity(tableName = "info")
@Parcelize
data class DBVenueInfo(
    @PrimaryKey(autoGenerate = false) val id: String = "",
    @ColumnInfo(name = "name") var name: String? = "",

    @TypeConverters(InfoConverter::class)
    @ColumnInfo(name = "photos") var photos: ArrayList<String>? = null,

    @ColumnInfo(name = "address") var address: String? = "",
    @ColumnInfo(name = "phone") var phone: String? = "",
    @ColumnInfo(name = "instagram") var instagram: String? = "",
    @ColumnInfo(name = "visit") var visitsCount: Long? = 0,
    @ColumnInfo(name = "likes") var likesCount: Long? = 0,
    @ColumnInfo(name = "here") var hereCount: Long? = 0,
    @ColumnInfo(name = "category") var category: String? = "",
) : Parcelable


class InfoConverter {
    @TypeConverter
    fun toPhotos(json: String): ArrayList<String> {
        val type = object : TypeToken<ArrayList<String>>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun toJson(photos: ArrayList<String>): String {
        val type = object: TypeToken<ArrayList<String>>() {}.type
        return Gson().toJson(photos, type)
    }
}

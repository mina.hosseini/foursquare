package com.foursquare.example.db.dao

import androidx.room.*
import com.foursquare.example.db.table.DBLocationList

@Dao
interface LocationListDao {

    @Query("SELECT location from locations")
    fun getAllLocations(): List<String>

    @Query("SELECT * from locations WHERE location = :location")
    suspend fun getLocationList(location: String): DBLocationList

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(locationList: DBLocationList)
}

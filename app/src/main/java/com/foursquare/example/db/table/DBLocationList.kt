package com.foursquare.example.db.table

import android.os.Parcelable
import androidx.room.*
import com.foursquare.example.model.VenuesExploreRes
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.parcelize.Parcelize

@Entity(tableName = "locations")
@Parcelize
data class DBLocationList(

    @PrimaryKey(autoGenerate = false) val page: Int = 0,

    @ColumnInfo(name = "location") var location: String? = null,

    @TypeConverters(Converter::class)
    @ColumnInfo(name = "locationList") var locationList:VenuesExploreRes? = null

) : Parcelable

class Converter {
    @TypeConverter
    fun toVenue(json: String):VenuesExploreRes {
        val type = object : TypeToken<VenuesExploreRes>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun toJson(venue:VenuesExploreRes): String {
        val type = object: TypeToken<VenuesExploreRes>() {}.type
        return Gson().toJson(venue, type)
    }
}

package com.foursquare.example.db

import android.content.Context
import androidx.room.*
import com.foursquare.example.db.dao.*
import com.foursquare.example.db.table.*

@Database(
    entities = arrayOf( DBVenueInfo::class, DBLocationList::class),
    version = 1,
    exportSchema = false
)
@TypeConverters(Converter::class, InfoConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun venueInfoDao(): VenueInfoDao
    abstract fun locationListDao(): LocationListDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "database"
                ).allowMainThreadQueries().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

package com.foursquare.example.fragment.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.foursquare.example.R
import com.foursquare.example.databinding.FragmentHomeBinding
import com.foursquare.example.extensions.observe
import com.foursquare.example.fragment.ArchBaseFragment
import com.foursquare.example.model.GroupItem
import com.foursquare.example.util.*
import com.foursquare.example.view.adapter.*
import com.foursquare.example.view.cell.VenuesCell
import kotlinx.coroutines.*


@FlowPreview
@ExperimentalCoroutinesApi
class HomeFragment : ArchBaseFragment<FragmentHomeBinding, HomeFragmentVM>() , GpsUtils.onGpsListener  {

    override val viewModel: HomeFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_home

    lateinit var adapter: AppGenericAdapter
    lateinit var linearLayoutManager: LinearLayoutManager

    private val PAGE_START = 1
    private var TOTAL_PAGES = 50
    private var currentPage = PAGE_START
    private var isLastPage = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onBackPressedCallback.isEnabled = false
        getLastKnownLocation()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()
        setRecyclerViewAdapterAndListener()
    }

    private fun setRecyclerViewAdapterAndListener() {
        binding.isEmpty = false
        binding.recyclerView.apply {
            this@HomeFragment.adapter = AppGenericAdapter().apply {
                provider { ctx ->
                    VenuesCell(ctx) { venues ->
                        navigate(
                            HomeFragmentDirections.navigateHomeToVenueInfoFragment(venues.venue.id),
                            NavAnimations.leftToRight
                        )
                    }
                }
            }
            adapter = this@HomeFragment.adapter
        }

        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.layoutManager = linearLayoutManager
        binding.recyclerView.addOnScrollListener(object :
            PaginationScrollListener(linearLayoutManager) {
            override fun loadMoreItems() {
                currentPage += 1
                loadData()
            }

            override val totalPageCount get() = TOTAL_PAGES
            override val isLastPage: Boolean = false
            override var isLoading: Boolean = false
        })
    }

    override fun registerObservers() {

        observe(viewModel.venuesResponse) {

            if (it.isLoading()) {
                if (currentPage == PAGE_START)
                    binding.loadingFristPage = true
                else if (currentPage <= TOTAL_PAGES)
                    binding.loadingNextPage = true
                else isLastPage = true
            }

            if (it.isSuccess()) {
                val venuesInfo = it.data
                if (venuesInfo != null && venuesInfo.response.groups.get(0).items.isNotEmpty()) {
                    adapter.addSectionsAndNotify<VenuesCell, GroupItem>(venuesInfo.response.groups.get(0).items)
                } else if (currentPage == PAGE_START) {
                    binding.isEmpty = true
                }
                binding.loadingNextPage = false
                binding.loadingFristPage = false
            }

            if (it.isError()) {
                binding.loadingNextPage = false
                binding.loadingFristPage = false
                binding.isEmpty = true
                showSnackBar(it.message ?: "Unknown Error")
            }
        }

    }

    fun loadData() {
        if (getLastKnownLocation()!=null)
            viewModel.getVenues(getLastKnownLocation()!!, currentPage)
        else
            showSnackBar(getString(R.string.please_turn_on_gps_and_net))
    }

    fun turnOnGPS() {
        activity?.let {
            GpsUtils(it).turnGPSOn(this)
        }
    }

    override fun gpsStatus(isGPSEnable: Boolean) {
    }


}

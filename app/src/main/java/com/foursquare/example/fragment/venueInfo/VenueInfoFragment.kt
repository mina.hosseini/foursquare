package com.foursquare.example.fragment.venueInfo

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.foursquare.example.R
import com.foursquare.example.databinding.FragmentVenueInfoBinding
import com.foursquare.example.extensions.observe
import com.foursquare.example.fragment.ArchBaseFragment
import com.foursquare.example.view.adapter.AppGenericAdapter
import com.foursquare.example.view.cell.ImageCell
import com.google.android.material.tabs.TabLayoutMediator

class VenueInfoFragment : ArchBaseFragment<FragmentVenueInfoBinding, VenueInfoFragmentVM>() {

    override val viewModel: VenueInfoFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_venue_info
    private val args by navArgs<VenueInfoFragmentArgs>()
    lateinit var imageAdapter: AppGenericAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getVenueInfo(args.id)
    }

    override fun registerObservers() {
        observe(viewModel.venueInfoResponse) {

            if (it.isLoading()) {
                binding.loading = true
            }

            if (it.isSuccess()) {
                binding.loading = false
                binding.venue = it.data?.response?.venue
                it.data?.response?.let { venue ->
                    imageAdapter.addSectionsAndNotify<ImageCell, String>(venue.photoURLs)
                }
            }

            if (it.isError()) {
                binding.loading = false
                showSnackBar(it.message ?: "Unknown Error")
            }
        }
    }

    override fun onBindingCreated(savedInstanceState: Bundle?) {
        super.onBindingCreated(savedInstanceState)
        initVenuesViewPager()
    }

    private fun initVenuesViewPager() = with(binding.venueViewPager) {
        this@VenueInfoFragment.imageAdapter = AppGenericAdapter().apply {
            provider { ctx ->
                ImageCell(ctx)
            }
        }
        orientation = ViewPager2.ORIENTATION_HORIZONTAL
        adapter = imageAdapter
        TabLayoutMediator(binding.venueTabLayout, this) { tab, position -> }.attach()
    }

}
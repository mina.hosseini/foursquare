package com.foursquare.example.fragment

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.location.*
import android.net.*
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.annotation.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.*
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.foursquare.example.R
import com.foursquare.example.extensions.*
import com.foursquare.example.util.UiHelper
import com.google.android.material.snackbar.Snackbar
import com.phelat.navigationresult.BundleFragment
import kotlinx.coroutines.*
import java.util.concurrent.TimeoutException


abstract class ArchBaseFragment<B : ViewDataBinding, VM : ViewModel> : BundleFragment(),
    CoroutineScope by CoroutineScope(Dispatchers.Main) {

    @LayoutRes
    abstract fun layout(): Int
    abstract val viewModel: VM

    protected lateinit var binding: B
    private val REQUEST_LOCATION_PERMISSION = 1
    var lastKnownLoc: Location? = null

    @CallSuper
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, layout(), container, false)
        binding.lifecycleOwner = this
        binding.setView(this)
        binding.setVm(viewModel)
        onBindingCreated(savedInstanceState)
        registerObservers()

        return binding.root
    }

    open fun registerObservers() {}

    open fun onBindingCreated(savedInstanceState: Bundle?) {}

    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }

    operator fun <T> LiveData<T>.invoke(observer: ((T) -> Unit)) {
        observe(this, observer)
    }

    fun showSnackBar(message: String, @ColorRes bgColor: Int? = null): Snackbar {
        return UiHelper.showSnackbar(
            binding.root,
            message,
            bgColor = if (bgColor == null) null else ContextCompat.getColor(
                requireContext(),
                bgColor
            )
        )
    }

    protected val navController: NavController?
        get() {
            return try {
                findNavController()
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

    fun navigateUp() {
        navController?.navigateUp()
    }

    protected val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            findNavController().navigateUp()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)
    }

    open fun isNetworkConnected(): Boolean {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            } else {
                TODO("VERSION.SDK_INT < M")
            }
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }
        return false
    }

    fun fetchErrorMessage(throwable: Throwable): String? {
        var errorMsg = resources.getString(R.string.error_msg_unknown)
        if (!isNetworkConnected()) {
            errorMsg = resources.getString(R.string.error_msg_no_internet)
        } else if (throwable is TimeoutException) {
            errorMsg = resources.getString(R.string.error_msg_timeout)
        }
        return errorMsg
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                getLastKnownLocation()
        }
    }

    override fun startIntentSenderForResult(
        intent: IntentSender?,
        requestCode: Int,
        fillInIntent: Intent?,
        flagsMask: Int,
        flagsValues: Int,
        extraFlags: Int,
        options: Bundle?
    ) {
        super.startIntentSenderForResult(
            intent,
            requestCode,
            fillInIntent,
            flagsMask,
            flagsValues,
            extraFlags,
            options
        )
    }

    fun getLastKnownLocation(): Location? {
        val locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val locationProvider = LocationManager.NETWORK_PROVIDER

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                REQUEST_LOCATION_PERMISSION
            )
            return null
        } else {
            lastKnownLoc = locationManager.getLastKnownLocation(locationProvider)
            return lastKnownLoc
        }
    }

}

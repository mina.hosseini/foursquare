package com.foursquare.example.fragment.venueInfo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.foursquare.example.api.Resource
import com.foursquare.example.db.dao.VenueInfoDao
import com.foursquare.example.db.table.DBVenueInfo
import com.foursquare.example.di.inject
import com.foursquare.example.model.VenueInfoResponse
import com.foursquare.example.mvvmutils.BaseViewModel
import com.foursquare.example.repository.AppRepository
import com.foursquare.example.util.safeLet10
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class VenueInfoFragmentVM : BaseViewModel() {

    private val venueInfoDao: VenueInfoDao by inject()
    private val repository: AppRepository = AppRepository(venueInfoDao)

    val venueInfoResponse = MutableLiveData<Resource<VenueInfoResponse>>()

    fun getVenueInfo(id: String) = viewModelScope.launch {
        venueInfoResponse.value = Resource.loading()
        val response = repository.getVenueInfo(id)
        venueInfoResponse.value = response

        if (response.isSuccess())
            safeLet10(
                response.data?.response?.venue?.id,
                response.data?.response?.venue?.name?:"",
                response.data?.response?.photoURLs(),
                response.data?.response?.venue?.location?.address?:"",
                response.data?.response?.venue?.contact?.phone?:"",
                response.data?.response?.venue?.contact?.instagram?:"",
                response.data?.response?.venue?.stats?.visitsCount?:0,
                response.data?.response?.venue?.likes?.count?:0,
                response.data?.response?.venue?.hereNow?.count?:0,
                response.data?.response?.venue?.categories?.get(0)?.name?:""
            ) { id, name, photoURLs, address, phone, instagram, visitsCount, likesCount, hereCount, category ->
                insert(DBVenueInfo(id, name, photoURLs, address, phone, instagram, visitsCount, likesCount, hereCount, category))
            }
    }

    fun insert(venueInfo: DBVenueInfo) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertVenueInfo(venueInfo)
    }
}
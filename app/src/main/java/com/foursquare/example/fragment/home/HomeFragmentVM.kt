package com.foursquare.example.fragment.home

import android.location.Location
import androidx.lifecycle.*
import com.foursquare.example.api.Resource
import com.foursquare.example.db.dao.LocationListDao
import com.foursquare.example.db.table.DBLocationList
import com.foursquare.example.di.inject
import com.foursquare.example.model.VenuesExploreRes
import com.foursquare.example.mvvmutils.BaseViewModel
import com.foursquare.example.repository.*
import kotlinx.coroutines.*


@ExperimentalCoroutinesApi
@FlowPreview
class HomeFragmentVM : BaseViewModel() {

    private val locationListDao: LocationListDao by inject()
    private val repository: AppRepository = AppRepository(locationListDao)

    val venuesResponse = MutableLiveData<Resource<VenuesExploreRes>>()
    fun getVenues(location: Location, page: Int) = viewModelScope.launch {
        val ll = "${location.latitude},${location.longitude}"

        venuesResponse.value = Resource.loading()
        val response = repository.venuesExplore(ll,location, page)
        venuesResponse.value = response

        if (response?.isSuccess() == true)
            insert(DBLocationList(page,ll, response.data))
    }

    fun insert(locationList: DBLocationList) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(locationList)
    }
}
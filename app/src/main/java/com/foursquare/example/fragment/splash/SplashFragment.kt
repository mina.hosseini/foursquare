package com.foursquare.example.fragment.splash

import android.view.animation.AnimationUtils
import androidx.fragment.app.viewModels
import com.foursquare.example.R
import com.foursquare.example.databinding.FragmentSplashBinding
import com.foursquare.example.fragment.ArchBaseFragment
import com.foursquare.example.util.NavAnimations
import kotlinx.coroutines.*

class SplashFragment : ArchBaseFragment<FragmentSplashBinding, SplashFragmentVM>() {

    override val viewModel: SplashFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_splash

    override fun onResume() {
        super.onResume()
        playAnimation()
        CoroutineScope(Dispatchers.Main).launch {
            delay(3000)
            if (navController?.currentDestination?.id == R.id.splash)
                navigate(SplashFragmentDirections.navigateSplashToHome(), NavAnimations.leftToRight)
        }
    }

    private fun playAnimation() {
        val starAnim = AnimationUtils.loadAnimation(activity, R.anim.left)
        starAnim.duration = 500
        binding.txtNear.startAnimation(starAnim)

        val numAnim = AnimationUtils.loadAnimation(activity, R.anim.left)
        numAnim.duration = 950
        binding.txtEs.startAnimation(numAnim)

        val sharpAnim = AnimationUtils.loadAnimation(activity, R.anim.left)
        sharpAnim.duration = 1300
        binding.txtT.startAnimation(sharpAnim)
    }

}


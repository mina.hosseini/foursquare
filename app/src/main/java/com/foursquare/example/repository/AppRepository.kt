package com.foursquare.example.repository

import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.foursquare.example.api.*
import com.foursquare.example.app.Constants
import com.foursquare.example.db.dao.*
import com.foursquare.example.db.table.*
import com.foursquare.example.model.*
import com.foursquare.example.util.*


class AppRepository(private val locationListDao: LocationListDao?) {

    private var venueInfoDao: VenueInfoDao? = null
    
    constructor(venueInfoDao: VenueInfoDao) : this(null) {
        this.venueInfoDao = venueInfoDao
    }

    

    // Venue List in main page
    var venuesExplore = MutableLiveData<Resource<VenuesExploreRes>>()
    
    suspend fun venuesExplore(ll:String,location: Location, page: Int): Resource<VenuesExploreRes>? {

        val nearestLocation=getNearestLocation(location)
        nearestLocation?.let {
            locationListDao?.getLocationList("${it.latitude},${it.longitude}")?.locationList?.let {
                val cashedData: Resource<VenuesExploreRes> = Resource.success(
                    VenuesExploreRes(Response(arrayListOf()))
                )
                cashedData.data = it
                return cashedData
            }
        }

        val ans = apiCall {
            venuesExplore(
                Constants.CLIENT_ID, Constants.CLIENT_SECRET,
                Constants.FOURSQUARE_VERSION, ll, "1000", "20", page
            )
        }
        
        if (ans.isLoading()) {
            return Resource.loading()
        } else if (ans.isSuccess() && ans.data != null) {
            val list = ans.data
            venuesExplore.value = Resource.success(list)
            return venuesExplore.value!!
        }

        return Resource.error(ans.message, null, ans.errorObject)
    }
    
    suspend fun insert(locationList: DBLocationList) {
        locationListDao?.insert(locationList)
    }
    
    fun getNearestLocation(currentLocation: Location) :Location?{
        val cashedLocations: List<String>? = locationListDao?.getAllLocations()
        cashedLocations?.forEach {
            val cashLoc=it.split(",")
            val isClose=LocationUtil.areTwoLocationsClose(
                currentLocation.latitude,
                currentLocation.longitude,
                cashLoc.get(0).toDouble(),
                cashLoc.get(1).toDouble()
            )
            val loc = Location("")
            loc.latitude=cashLoc.get(0).toDouble()
            loc.longitude=cashLoc.get(1).toDouble()
            if (isClose)
                return loc
        }
        return null
    }

    

    // Venue Info in detail page
    val venueInfo = MutableLiveData<Resource<VenueInfoResponse>>()
    
    suspend fun getVenueInfo(id: String): Resource<VenueInfoResponse> {

        val cashedInfoData = venueInfoDao?.getVenueInfo(id)
        if (cashedInfoData != null) {
            val cashedData: Resource<VenueInfoResponse> = Resource.success(
                VenueInfoResponse(
                    InfoResponse(
                        InfoVenue(
                            "",
                            "",
                            Contact("", ""),
                            InfoLocation(""),
                            arrayListOf(),
                            InfoStats(0),
                            InfoLikes(0),
                            InfoHereNow(0),
                            Listed(arrayListOf()),
                            Photo("", "")
                        )
                    )
                )
            )

            safeLet10(
                id,
                cashedInfoData.name,
                cashedInfoData.photos,
                cashedInfoData.address,
                cashedInfoData.phone,
                cashedInfoData.instagram,
                cashedInfoData.visitsCount,
                cashedInfoData.likesCount,
                cashedInfoData.hereCount,
                cashedInfoData.category
            ) { id, name, photos, address, phone, instagram, visitsCount, likesCount, hereCount, category ->

                val categories = arrayListOf<InfoCategory>()
                categories.add(InfoCategory(category))

                cashedData.data =
                    VenueInfoResponse(
                        InfoResponse(
                            InfoVenue(
                                id,
                                name,
                                Contact(phone, instagram),
                                InfoLocation(address),
                                categories,
                                InfoStats(visitsCount),
                                InfoLikes(likesCount),
                                InfoHereNow(hereCount),
                                Listed(listOf()),
                                Photo("", "")
                            )
                        )
                    )

                cashedData.data?.response?.photoURLs = photos
            }

            return cashedData
        }
        
        val ans = apiCall {
            getVenueInfo(
                id,
                Constants.CLIENT_ID,
                Constants.CLIENT_SECRET,
                Constants.FOURSQUARE_VERSION
            )
        }

        if (ans.isLoading()) {
            return Resource.loading()
        } else if (ans.isSuccess()) {
            val venue = ans.data
            venueInfo.value = Resource.success(venue)
            return venueInfo.value!!
        }
        return Resource.error(ans.message, null, ans.errorObject)
    }

    suspend fun insertVenueInfo(venueInfo: DBVenueInfo) {
        venueInfoDao?.insert(venueInfo)
    }

}

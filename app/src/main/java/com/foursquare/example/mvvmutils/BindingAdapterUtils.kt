@file:Suppress("unused")

package com.foursquare.example.mvvmutils

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.foursquare.example.util.PixelUtil
import com.foursquare.example.util.RoundedCornersTransformation
import com.squareup.picasso.Picasso


object BindingAdapterUtils {

    @JvmStatic
    @BindingAdapter(value = ["loadFromUrl", "placeholder", "radius", "margin"], requireAll = false)
    fun setImageUrl(
        imageView: ImageView,
        url: String?,
        drawable: Drawable?,
        radius: Int = 0,
        margin: Int = 0
    ) {
        if (!url.isNullOrEmpty()) {

            val mRadius = PixelUtil.dpToPx(radius.toFloat())

            val loader = Picasso.get().load(
                    url
            )
            if (mRadius > 0)
                loader.transform(RoundedCornersTransformation(mRadius, 0))
            if (drawable != null)
                loader.placeholder(drawable)
            loader.into(imageView)
        } else {
            imageView.setImageDrawable(drawable)
        }
    }

}
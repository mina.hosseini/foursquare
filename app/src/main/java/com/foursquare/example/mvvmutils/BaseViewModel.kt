package com.foursquare.example.mvvmutils

import androidx.lifecycle.*
import kotlinx.coroutines.*

abstract class BaseViewModel : ViewModel(), CoroutineScope by CoroutineScope(Dispatchers.Main)
package com.foursquare.example.view.cell

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import com.foursquare.example.R
import com.foursquare.example.databinding.CellImageBinding
import com.foursquare.example.view.BaseCustomView
import com.foursquare.example.view.adapter.GenericAdapterView


@SuppressLint("ViewConstructor")
class ImageCell : BaseCustomView<CellImageBinding>, GenericAdapterView<String> {

    override fun layout(): Int = R.layout.cell_image

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    )

    init {
        setWillNotDraw(false)
    }

    override fun onBind(model: String, position: Int, extraObject: Any?) {
        binding.imageURL = model
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        layoutParams.width = LayoutParams.MATCH_PARENT
        layoutParams.height = LayoutParams.MATCH_PARENT
    }

}
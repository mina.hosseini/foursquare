package com.foursquare.example.view.cell

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.foursquare.example.R
import com.foursquare.example.view.adapter.GenericAdapterView
import com.foursquare.example.databinding.CellVenuesBinding
import com.foursquare.example.model.GroupItem
import com.foursquare.example.view.BaseCustomView

@SuppressLint("ViewConstructor")
class VenuesCell : BaseCustomView<CellVenuesBinding>, GenericAdapterView<GroupItem> {

    var backGrounds = arrayOf(
        ContextCompat.getDrawable(context, R.drawable.yellow_backgroung),
        ContextCompat.getDrawable(context, R.drawable.green_backgroung),
        ContextCompat.getDrawable(context, R.drawable.blue_backgroung),
        ContextCompat.getDrawable(context, R.drawable.pink_backgroung),
        ContextCompat.getDrawable(context, R.drawable.purple_backgroung)
    )

    var onItemClick: ((item: GroupItem) -> Unit)? = null

    override fun layout(): Int = R.layout.cell_venues

    constructor(context: Context) : super(context)

    constructor(
        context: Context,
        onItemClick: (freewayToll: GroupItem) -> Unit,
    ) : super(context) {
        this.onItemClick = onItemClick
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    fun onClick() {
        binding.item?.let {
            onItemClick?.invoke(it)
        }
    }

    override fun onBind(model: GroupItem, position: Int, extraObject: Any?) {
        binding.item = model
        binding.icon.background = backGrounds[position % 5]
    }

}